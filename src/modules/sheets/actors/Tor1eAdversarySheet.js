import {tor1eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor1eAdversarySheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 650,
            height: 775,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/adversary-sheet.hbs`
        });
    }

    async getData() {
        const baseData = super.getData();
        let constants = CONFIG.tor1e.constants;

        let items = baseData.items.map(i => this.actor.items.get(i._id));

        let [leftSpecialAbilities, rightSpecialAbilities] =
            tor1eUtilities.filtering.getAndSplitItemsBy(items, constants.specialAbility);
        let shield = tor1eUtilities.filtering.getItemBy(items, constants.armour, constants.shield);
        let headgear = tor1eUtilities.filtering.getItemBy(items, constants.armour, constants.headgear);
        let armour = tor1eUtilities.filtering.getItemIn(items, constants.armour, [constants.mailArmour, constants.leatherArmour]);
        let weapons = tor1eUtilities.filtering.getItemsBy(items, constants.weapon);
        await Promise.all(weapons.map(
            async weapon => weapon["description"] = await TextEditor.enrichHTML(weapon.system.description.value, {async: true})
        ));

        return {
            description: await TextEditor.enrichHTML(this.object.system.description.value, {async: true}),
            owner: this.actor.isOwner,
            system: baseData.actor.system,
            actor: baseData.actor,
            config: CONFIG.tor1e,
            backgroundImages: CONFIG.tor1e.backgroundImages["adversary"],
            leftSpecialAbilities: leftSpecialAbilities,
            rightSpecialAbilities: rightSpecialAbilities,
            weapons: weapons,
            shieldDTO: {
                id: shield ? shield.id : 0,
                name: shield ? shield.name : game.i18n.localize("tor1e.actors.stats.noShield"),
                value: shield ? shield.system.protection.value : 0,
            },
            headgearDTO: {
                id: headgear ? headgear.id : 0,
                name: headgear ? headgear.name : game.i18n.localize("tor1e.actors.stats.noHeadgear"),
                value: headgear ? headgear.system.protection.value : 0,
            },
            armourDTO: {
                id: armour ? armour.id : 0,
                css: "dice",
                name: armour ? armour.name : game.i18n.localize("tor1e.actors.stats.noArmour"),
                value: armour ? armour.system.protection.value : 0,
                favoured: {
                    state: this.actor.system.armour.favoured.value,
                    name: "data.armour.favoured.value",
                    value: 0
                },
                roll: {
                    label: "tor1e.rolls.protection",
                    associatedAttribute: "body",
                    bonus: headgear ? headgear.system.protection.value : 0
                }
            },
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
            }
        };
    }

    /**
     * Sets up the data transfer within a drag and drop event. This function is triggered
     * when the user starts dragging an inventory item, and dataTransfer is set to the
     * relevant data needed by the _onDrop function. See that for how drop events
     * are handled.
     *
     * @private
     *
     * @param {Object} event    event triggered by item dragging
     */
    _onDragCombatSkillStart(event) {
        let itemId = event.currentTarget.getAttribute("data-item-id");
        const initialItem = this.actor.items.get(itemId);
        const item = duplicate(initialItem)
        event.dataTransfer.setData("text/plain", JSON.stringify({
            type: "Item",
            actorId: this.actor.id,
            data: item,
            itemId: initialItem.uuid,
        }));
    }

    activateListeners(html) {

        // Combat Dragging
        let combatSkillHandler = ev => this._onDragCombatSkillStart(ev);
        html.find('.item-draggable').each((i, li) => {
            li.setAttribute("draggable", true);
            li.addEventListener("dragstart", combatSkillHandler, false);
        });

        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor1eEffect").click(tor1eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".item-delete").click(tor1eUtilities.eventsProcessing.onItemDelete.bind(this));
        html.find(".item-edit").click(tor1eUtilities.eventsProcessing.onItemEdit.bind(this));
        html.find(".inline-edit").change(tor1eUtilities.eventsProcessing.onSkillEdit.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));


        // Owner-only listeners
        if (this.actor.isOwner) {
            let extra = {
                "favouredSkillValue": this.actor.system.attributeLevel.value,
                "shadowServant": true,
            }
            html.find(".inline-skill-modify").click(tor1eUtilities.eventsProcessing.onSkillModify.bind(this, {}));
            html.find(".inline-item-skill-modify").click(tor1eUtilities.eventsProcessing.onItemSkillModify.bind(this, {}));
            html.find(".skill-name").click(tor1eUtilities.eventsProcessing.onSkillName.bind(this, extra));
            html.find(".weapon-name").click(tor1eUtilities.eventsProcessing.onItemName.bind(this, extra));
        }
        super.activateListeners(html);
    }

}