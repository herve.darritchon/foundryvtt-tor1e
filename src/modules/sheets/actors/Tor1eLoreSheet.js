import {tor1eUtilities} from "../../utilities.js";
import {StatusEffects} from "../../effects/status-effects.js";

export default class Tor1eLoreSheet extends ActorSheet {
    /** @override */
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["tor1e", "sheet", "actor"],
            width: 450,
            height: 525,
            template: `${CONFIG.tor1e.properties.rootpath}/templates/sheets/actors/lorecharacter-sheet.hbs`
        });
    }

    async getData() {
        const baseData = super.getData();
        baseData.config = CONFIG.tor1e;
        baseData.backgroundImages = CONFIG.tor1e.backgroundImages["lore"];

        baseData.effects = {
            "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
            "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
            "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
        }

        return {
            description: await TextEditor.enrichHTML(this.object.system.description.value, {async: true}),
            notes: await TextEditor.enrichHTML(this.object.system.notes.value, {async: true}),
            owner: this.actor.isOwner,
            system: baseData.actor.system,
            actor: baseData.actor,
            config: CONFIG.tor1e,
            backgroundImages: CONFIG.tor1e.backgroundImages["lore"],
            effects: {
                "weary": this.actor.buildStatusEffectById(StatusEffects.WEARY),
                "wounded": this.actor.buildStatusEffectById(StatusEffects.WOUNDED),
                "poisoned": this.actor.buildStatusEffectById(StatusEffects.POISONED),
            }
        };
    }

    activateListeners(html) {
        /*
            code pattern
            html.find(cssSelector).event(this._someCallBack.bind(this));
         */
        html.find(".toggleTor1eEffect").click(tor1eUtilities.eventsProcessing.onToggleEffect.bind(this));
        html.find(".toggle").click(tor1eUtilities.eventsProcessing.onToggle.bind(this));
        html.find(".editor-toggle").click(tor1eUtilities.eventsProcessing.onEditorToggle.bind(this));

        super.activateListeners(html);
    }

}