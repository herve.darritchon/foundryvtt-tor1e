import * as Tor1eDie from "../die.js"
import {Tor1eRoll} from "../Tor1eRoll.js";
import Tor1eChatMessage from "../chat/Tor1eChatMessage.js";

async function getTaskCheckOptions(taskType, difficulty, formula) {
    const template = `${CONFIG.tor1e.properties.rootpath}/templates/chat/task-check-dialog.hbs`;
    let backgroundImages = CONFIG.tor1e.backgroundImages["dice-roll"];
    const html = await renderTemplate(template, {
        difficulty: difficulty,
        featDice: false,
        bonusDie: false,
        masteryDice: 0,
        backgroundImages: backgroundImages,
        formula: formula
    });

    return new Promise(resolve => {
        const data = {
            title: game.i18n.format("tor1e.chat.taskCheck.title", {type: taskType}),
            content: html,
            buttons: {
                normal: {
                    label: game.i18n.localize("tor1e.chat.actions.roll"),
                    callback: html => resolve(_processTaskCheckOptions(html[0].querySelector("form")))
                },
                cancel: {
                    label: game.i18n.localize("tor1e.chat.actions.cancel"),
                    callback: html => resolve({cancelled: true})
                }
            },
            default: "normal",
            close: () => resolve({cancelled: true})

        }
        const options = {
            width: 450,
            height: 350,
        }
        new Dialog(data, options).render(true)
    });
}

function _processTaskCheckOptions(form) {
    return {
        difficulty: parseInt(form.difficulty.value),
        featDiceAdded: form.featDice.checked ? 1 : 0,
        bestFeatDie: !form.bestFeatDie.checked,
        bonusDiceAdded: form.bonusDie.checked ? 1 : 0,
        masteryDiceAdded: parseInt(form.masteryDice.value),
    }
}

export async function taskCheck(
    {
        actor = null,
        user = null,
        actionValue = null,
        actionName = "",
        difficulty = 14,
        featDiceValue = 1,
        bonusDiceAdded = 0,
        masteryDiceAdded = 0,
        featDiceAdded = 0,
        bestFeatDie = true,
        wearyRoll = false,
        shadowServant = false,
        askForOptions = true,
        taskType = actionName || "Standard",
        type = "Standard",
        modifier = 0,
        hopeModifier = 0,
        hopePoint = 0,
        hopePointPostAction = {},
    } = {}) {

    let nbDicePonderated = actionValue;

    if (askForOptions) {
        let nbSuccessDice = parseInt(actionValue);
        shadowServant = actor.extendedData.isHostile;
        let featDieObject = shadowServant ?
            {
                img: Tor1eDie.TORSauronicFeatBaseDie.IMG,
                value: featDiceValue
            } :
            {
                img: Tor1eDie.TORFeatBaseDie.IMG,
                value: featDiceValue
            };
        let formula = {
            feat: featDieObject,
            success: {
                display: nbSuccessDice > 0,
                img: wearyRoll ? Tor1eDie.TORWearySuccessDie.IMG : Tor1eDie.TORSuccessDie.IMG,
                value: nbSuccessDice
            },
            modifier: {
                display: modifier && modifier !== 0,
                value: modifier
            }
        }

        let checkOptions = await getTaskCheckOptions(taskType, difficulty, formula);

        if (checkOptions.cancelled) {
            return;
        }

        difficulty = checkOptions.difficulty || 14;
        bonusDiceAdded = checkOptions.bonusDiceAdded;
        masteryDiceAdded = checkOptions.masteryDiceAdded || 0;
        featDiceAdded = checkOptions.featDiceAdded;
        bestFeatDie = checkOptions.bestFeatDie;
    }

    Object.defineProperty(String.prototype, "sanityze", {
        value: function sanityze() {
            return this.replace(/^\r?\n|\r/, "");
        },
        writable: true,
        configurable: true
    });

    function _buildRollFormula() {
        let nbDiceBase = parseInt(actionValue);
        nbDicePonderated = nbDiceBase + bonusDiceAdded > 6 ? 6 : nbDiceBase + bonusDiceAdded;
        let nbSuccessDice = nbDicePonderated + masteryDiceAdded;
        let successDiceSuffix = masteryDiceAdded > 0 ? `dl${masteryDiceAdded}` : "";
        let nbFeatDice = featDiceValue + featDiceAdded;
        let featDiceSuffix = "";
        if (featDiceAdded > 0) {
            featDiceSuffix = bestFeatDie ? "kh" : "kl";
        }
        let bonus = (modifier !== 0) ? ` + ${modifier}` : ""
        let baseDice = wearyRoll ? wearyDie : standardDie;
        let successDice = nbSuccessDice > 0 ? ` + (${nbSuccessDice})${baseDice}` : ""
        let featDiceType = shadowServant ? sauronicFeatDie : featDie
        return `(${nbFeatDice})${featDiceType}${featDiceSuffix}${successDice}${successDiceSuffix}${bonus}`;
    }

    let standardDie = Tor1eDie.TORSuccessDie.COMMAND;
    let wearyDie = Tor1eDie.TORWearySuccessDie.COMMAND;
    let featDie = Tor1eDie.TORFeatBaseDie.COMMAND;
    let sauronicFeatDie = Tor1eDie.TORSauronicFeatBaseDie.COMMAND;

    let canSpendHopePoint = actor.extendedData.isCharacter;

    let rollFormula = _buildRollFormula();
    let rollData = {
        formula: rollFormula,
        actionValue: nbDicePonderated,
        masteryDiceAdded: masteryDiceAdded,
        difficulty: difficulty,
        modifier: modifier,
        hopeModifier: hopeModifier,
        canSpendHopePoint: canSpendHopePoint && (hopePoint > 0),
        bestFeatDie: bestFeatDie,
        flavor: {
            user: user.id,
            action: actionName,
            owner: {
                id: actor.id,
                img: actor.img,
                name: actor.name
            }
        },
        shadowServant: shadowServant,
        hopePointPostAction: hopePointPostAction,
    };

    let roll = new Tor1eRoll(rollFormula, rollData);
    let rollResult = await roll.roll({async: false});

    let speaker
    if (actor.extendedData.isCharacter) {
        speaker = ChatMessage.getSpeaker({token: actor.token, actor: actor});
    } else {
        speaker = ChatMessage.getSpeaker();
    }

    let messageData = {
        speaker: speaker,
        typeOfRoll: type,
        flags: Tor1eChatMessage.buildExtendedDataWith({
            roll: rollResult,
            hopePointPostAction: hopePointPostAction
        })
    }

    await rollResult.toMessage(messageData);

    return rollResult;
}