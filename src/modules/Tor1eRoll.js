import {TORSuccessDie, TORFeatBaseDie, TORSauronicFeatBaseDie, TORWearySuccessDie} from "./die.js";
import {tor1eSystemProperties} from "./system-properties.js";
import {tor1eRollDialog} from "./chat/rollDialog.js"
import {
    Tor1eRollAutomaticSuccess,
    Tor1eRollExtraordinarySuccess,
    Tor1eRollFailure,
    Tor1eRollGreatSuccess, Tor1eRollHidden, Tor1eRollResult,
    Tor1eRollSuccess
} from "./roll/Tor1eRollResult.js";
import Tor1eChatMessage from "./chat/Tor1eChatMessage.js";

export class Tor1eRoll extends Roll {

    isCustom = true;

    constructor(...args) {
        super(...args);
    }

    isSuccess() {
        return this.customResult.result.type.isSuccess;
    }

    isFailure() {
        return this.customResult.result.type.isFailure;
    }

    /**
     * @override
     * @returns {Tor1eRoll}
     */
    evaluate(async) {
        super.evaluate(async);
        if (this.formula) {
            // this.formula exists so it is a TOR1e roll and not a custom roll
            this.isCustom = false;
            this.customResult = this._computeRollResult(this.data.difficulty, this.data.shadowServant, this)
        }
        return this;
    }

    /**
     * This class is evaluated before the Tor1e system is setup so you can't use CONFIG object to get the value.
     * It's why, you have to use tor1eSystemProperties.path.root instead.
     * @type {string}
     */
    static CHAT_TEMPLATE = `${tor1eSystemProperties.path.root}/templates/sheets/messages/partials/common/roll-tor1e.hbs`;
    static TOOLTIP_TEMPLATE = `${tor1eSystemProperties.path.root}/templates/sheets/messages/partials/common/tooltip-tor1e.hbs`;

    /**
     * Render a Roll instance to HTML
     * This is the fvtt original implementation of the method + add somestuff for the TOR system
     * for the customer Total Amount
     * @param chatOptions {Object}      An object configuring the behavior of the resulting chat message.
     * @return {Promise.<HTMLElement>}  A Promise which resolves to the rendered HTML
     */
    /** @override */
    async render(chatOptions = {}) {
        chatOptions = mergeObject({
            user: game.user.id,
            flavor: this.flavor,
            difficulty: this.difficulty,
            template: this.constructor.CHAT_TEMPLATE,
            blind: false,
            shadowServant: this.shadowServant
        }, chatOptions);
        const isPrivate = chatOptions.isPrivate;

        // Execute the roll, if needed
        if (!this._evaluated) await this.roll({async: true}).then(roll => game.dice3d.showForRoll(roll));
        const chatData = await this._prepareChatData(chatOptions, isPrivate);

        // Render the roll display template
        return await renderTemplate(chatOptions.template, chatData);
    }

    async _prepareChatData(chatOptions, isPrivate) {
        const hiddenString = "***";
        const hiddenFlavor = {
            owner: {
                id: chatOptions.flavor && chatOptions.flavor.owner ? chatOptions.flavor.owner.id : game.user.id,
                img: "icons/svg/mystery-man.svg",
                name: isPrivate ? "mystery man" : game.user.name,
            },
            action: game.i18n.localize("tor1e.chat.actions.hidden"),
            value: 0,
        }
        return {
            formula: isPrivate ? hiddenString : await this._createDisplayFormula(),
            flavor: isPrivate || this.isCustom ? hiddenFlavor : chatOptions.flavor,
            difficulty: isPrivate || this.isCustom ? hiddenString : this.difficulty,
            hopeModifier: this.data.hopeModifier,
            canSpendHopePoint: isPrivate || this.isCustom ? false : this.customResult.result.type.isFailure
                && this.data.canSpendHopePoint
                && this.total < chatOptions.difficulty
                && this.total + this.data.hopeModifier >= chatOptions.difficulty
                && (this.flavor.user === game.user.id || game.user.isGM),
            user: isPrivate ? hiddenString : chatOptions.user,
            tooltip: isPrivate ? "" : await this.getTooltip(),
            total: isPrivate ? "?" : Math.round(this.total * 100) / 100,
            isCustom: this.isCustom,
            result: isPrivate || this.isCustom ? Tor1eRollResult.build(new Tor1eRollHidden()) : this.customResult.result,
        };
    }

    static async createModifiedRollBy(attribute, rollData) {
        let roll = await Tor1eRoll.fromData(rollData);
        roll._formula = roll._formula + " + " + attribute;
        roll._total = roll._total + attribute;
        roll.customResult = roll._computeRollResult(roll.data.difficulty, roll.data.shadowServant, roll)
        return roll;
    }

    /**
     * Activate event listeners using the chat log html.
     * @param html {HTML}  Chat log html
     */
    static async chatListeners(html) {

        // Change card to edit mode
        html.on('click', '.roll-action', event => {
            event.preventDefault();
            event.stopPropagation();
            if (event.altKey) {
                let element = event.currentTarget;
                let messageId = element.closest(".message").dataset.messageId;
                let message = game.messages.get(messageId);
                let tokenId = message.speaker.token;
                let actorId = message.speaker.actor;
                let actor, token;
                if (tokenId) token = canvas.tokens.get(tokenId);
                if (token) {
                    actor = token.actor;
                } else {
                    actor = game.actors.get(actorId);
                }
                if (!actor) {
                    ui.notifications.error(game.i18n.format("tor1e.chat.error.actorNotFound"),
                        {
                            tokenId: tokenId,
                            actorId: actorId,

                        });
                    return;
                } else if (!actor.extendedData.isCharacter) {
                    ui.notifications.warn(game.i18n.format("tor1e.chat.warn.characterHasNotBeenUsedToCreateTheAction"),
                        {
                            actorName: actor.name,
                        });
                    return;
                }
                let data = Tor1eChatMessage.getExtendedData(message);
                let modifier = parseInt(element.dataset.rollHopeModifier);

                tor1eRollDialog.utilities.buildRollChatMessage(data, actor, modifier);
            }
        });

    }

    async _createDisplayFormula() {
        let featDie = this.dice.find(die => die instanceof TORFeatBaseDie || die instanceof TORSauronicFeatBaseDie);
        let successDice = this.dice.find(die => !(die instanceof TORFeatBaseDie || die instanceof TORSauronicFeatBaseDie));
        if (!featDie || !successDice) {
            return await this.formula;
        }
        let featCls = featDie.constructor;
        let successCls = successDice.constructor;
        return await renderTemplate("systems/tor1e/templates/roll/display-formula-card.hbs", {
            feat: {
                img: featCls.IMG,
                value: featDie.number
            },
            success: {
                display: successDice.number > 0,
                img: successCls.IMG,
                value: successDice.number
            },
            modifier: {
                display: this.modifier && this.modifier !== 0,
                value: this.modifier
            }
        });
    }

    /**
     * Asses the roll result and display the right message
     * depending on the dice and the difficulty
     * @param difficulty
     * @param shadowServant
     * @param roll
     * @returns {{result: {cssClass: string, message: string}}}
     * @private
     */
    _computeRollResult(difficulty, shadowServant, roll = {}) {
        if (roll.total >= difficulty) {
            // Roll passes the difficulty
            let nbOfTengwars = this._rollNbOfTengwarRunes()
            if (nbOfTengwars === 0) {
                return Tor1eRollResult.build(new Tor1eRollSuccess());
            } else if (nbOfTengwars === 1) {
                return Tor1eRollResult.build(new Tor1eRollGreatSuccess());
            } else {
                return Tor1eRollResult.build(new Tor1eRollExtraordinarySuccess());
            }
        } else {
            // Roll doesn't pass the difficulty

            // Roll a failure but can be an automatic success with Gandalf rune
            let isFeatResult = this._isFeatResult(shadowServant);
            if (isFeatResult === true) {
                let nbOfTengwars = this._rollNbOfTengwarRunes()
                if (nbOfTengwars === 0) {
                    return Tor1eRollResult.build(new Tor1eRollAutomaticSuccess());
                } else if (nbOfTengwars === 1) {
                    return Tor1eRollResult.build(new Tor1eRollGreatSuccess());
                } else {
                    return Tor1eRollResult.build(new Tor1eRollExtraordinarySuccess());
                }
            } else {
                return Tor1eRollResult.build(new Tor1eRollFailure());
            }
        }
    }

    /**
     * Return if you roll a feat result :
     * - eye for adversary
     * - gandalf for companion
     * @param shadowServant
     * @returns {boolean}
     * @private
     */
    _isFeatResult(shadowServant) {
        if (shadowServant) {
            return this._rollAnEyeOfSauron()
        } else {
            return this._rollAGandalfRune()
        }
    }

    /**
     * Return if you roll a Rune of Gandalf
     * @returns {boolean}
     * @private
     */
    _rollAGandalfRune() {
        return this._filterSpecificDieAndValue(12, [TORFeatBaseDie]) >= 1;
    }

    /**
     * Return if you roll an Eye of Sauron
     * @returns {boolean}
     * @private
     */
    _rollAnEyeOfSauron() {
        return this._filterSpecificDieAndValue(11, [TORSauronicFeatBaseDie]) >= 1;
    }

    /**
     * Return the number of Tengwar runes on Success Dice
     * @returns {*}
     * @private
     */
    _rollNbOfTengwarRunes() {
        return this._filterSpecificDieAndValue(6, [TORWearySuccessDie, TORSuccessDie]);
    }

    /**
     * Return The number of specific die that rolled a specific value
     * e.g.: If you roll [6,2,2] for WearyDie, the function with parameter (6,[TorWearyBaseDie]) should return [6]
     * @param valueToFilter -> The value of the Die you want to filter
     * @param dicezz --> The list of Dice classes you want to filter
     * @returns {*}
     * @private
     */
    _filterSpecificDieAndValue(valueToFilter, dicezz) {
        return this.dice
            .filter(die => this._oneOfClasses(dicezz, die))
            .flatMap(r => r.results
                .map(result => result.result))
            .filter(value => value === valueToFilter)
            .length;
    }

    _oneOfClasses(dicezz, die) {
        return dicezz.map(clazz => die instanceof clazz).reduce((a, b) => a || b, false);
    }

    /* -------------------------------------------- */

    /**
     * Render the tooltip HTML for a Roll instance
     * @return {Promise<HTMLElement>}
     */
    /** @override */
    async getTooltip() {
        const parts = this.dice.map(die => {
            const cls = die.constructor;
            const data = this;
            return {
                formula: cls.dieLabel ? `${die.number} ${cls.dieLabel}` : die.expression,
                total: die.total,
                faces: die.faces,
                flavor: die.flavor,
                data: data,
                rolls: die.results
                    .sort((a, b) => {
                        if (a.active) return -1;
                        return 1;
                    })
                    .map((result, index) => {
                        const hasSuccess = result.success !== undefined;
                        const hasFailure = result.failure !== undefined;
                        const isMax = result.initialResult === die.faces;
                        const isMin = result.initialResult === 0;
                        let resultLabelFunction = cls?.getResultLabel;
                        let resultLabel, rollName;
                        if (typeof resultLabelFunction === "function") {
                            resultLabel = resultLabelFunction?.(result.result) ?? "standard";
                            rollName = cls?.name.toLowerCase();
                        } else {
                            resultLabel = "";
                            rollName = "standard";
                        }
                        return {
                            result: resultLabel,
                            classes: [
                                rollName,
                                "die" + die.faces,
                                result.success ? "success" : null,
                                result.failure ? "failure" : null,
                                result.rerolled ? "rerolled" : null,
                                result.exploded ? "exploded" : null,
                                result.discarded ? "discarded" : null,
                                !(hasSuccess || hasFailure) && isMin ? "min" : null,
                                !(hasSuccess || hasFailure) && isMax ? "max" : null
                            ].filter(c => c).join(" ")
                        }
                    })
            };
        });
        return await renderTemplate(this.constructor.TOOLTIP_TEMPLATE, {parts});
    }

    /* -------------------------------------------- */
    /** @override */
    async toMessage(messageData = {}, {rollMode = null, create = true} = {}) {
        // Perform the roll, if it has not yet been rolled
        if (!this._evaluated) this.evaluate(true);

        const rMode = rollMode || messageData.rollMode || game.settings.get("core", "rollMode");

        let template = CONST.CHAT_MESSAGE_TYPES.ROLL;
        if (["gmroll", "blindroll"].includes(rMode)) {
            messageData.whisper = ChatMessage.getWhisperRecipients("GM");
        }
        if (rMode === "blindroll") messageData.blind = true;
        if (rMode === "selfroll") messageData.whisper = [game.user.id];

        // Prepare chat data
        messageData = mergeObject(
            {
                user: game.user.id,
                type: template,
                content: this._total,
                sound: CONFIG.sounds.dice,
                flags: Tor1eChatMessage.buildExtendedDataWith({
                    flavor: {
                        type: messageData.typeOfRoll
                    }
                })
            },
            messageData
        );
        messageData.flags.tor1e.roll = this;
        messageData.roll = this;

        // Prepare message options
        const messageOptions = {rollMode: rMode};

        // Either create the message or just return the chat data
        return create ? await CONFIG.ChatMessage.documentClass.create(messageData, messageOptions) : messageData;
    }

    /** @override */
    toJSON() {
        const json = super.toJSON();
        json.customResult = this.customResult;
        json.isCustom = this.isCustom;
        json.actionValue = this.data.actionValue;
        json.difficulty = this.data.difficulty;
        json.flavor = this.data.flavor;
        json.data = this.data;
        return json;
    }

    /** @override */
    static fromData(data) {
        const roll = super.fromData(data);
        roll.customResult = data.customResult;
        roll.isCustom = data.isCustom;
        roll.actionValue = data.data.actionValue;
        roll.difficulty = data.data.difficulty;
        roll.flavor = data.flavor;
        roll.data = data.data;
        return roll;
    }

}