import * as chatHooks from "../chat/chat.js"
import * as hotbarHooks from "../macro/hotbarDrop.js"
import {Tor1eTokenHudExtension} from "../hud/Tor1eTokenHudExtension.js";
import Tor1eCombatant from "../combat/Tor1eCombatant.js";

export default function registerHooks() {
    Tor1eTokenHudExtension.default();
    chatHooks.default();
    hotbarHooks.default();
}