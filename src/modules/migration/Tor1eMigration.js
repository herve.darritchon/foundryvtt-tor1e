export default class Tor1eMigration {
    systemVersion = game.system.version;
    olderCompatibleVersion = "0.0.20"
    dataStructureVersion = game.settings.get("tor1e", "systemMigrationVersion") || this.olderCompatibleVersion;

    static async processMigrationScripts(scripts) {
        function _mergeResult(acc, newResult) {
            return {
                result: acc.result && newResult.result,
                migration: acc.migration || newResult.migration
            }
        }

        let reducer = async (accumulator, script) => _mergeResult(accumulator, await script.migrateWorld());
        return await scripts.reduce(reducer, {result: true, migration: false});
    }

    async migrateWorld(migrationDataFn, migrationScriptVersion, migration) {
        if (!this.canBeMigrated()) return {result: true, migration: false};

        if (!this.versionNeedsMigration(migrationScriptVersion)) return {result: true, migration: false};

        ui.notifications.info(`Applying Tor1e System Migration (${this.systemVersion}) for version ${migrationScriptVersion}. Please be patient and do not close your game or shut down your server.`, {permanent: true});

        let fnResult = await migrationDataFn(migration);

        ui.notifications.info(`Migration (${migrationScriptVersion}) has been applied to the World !`, {permanent: true});

        await this.setDataStructureVersion(migrationScriptVersion);

        return {result: fnResult, migration: true};
    }

    canBeMigrated() {
        if (this.systemVersion && isNewerVersion(this.systemVersion, this.olderCompatibleVersion)) {
            return true;
        } else {
            const warning = `Your Tor1e system data (${thisStructureVersion}  is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`;
            ui.notifications.warn(warning, {permanent: true});
            return false;
        }
    }

    versionNeedsMigration(migrationScriptVersion) {
        return this.dataStructureVersion && isNewerVersion(migrationScriptVersion, this.dataStructureVersion);
    }

    async setDataStructureVersion(version) {
        if (game.settings.get("tor1e", "systemMigrationVersion") === undefined || isNewerVersion(version, game.settings.get("tor1e", "systemMigrationVersion"))) {
            await game.settings.set("tor1e", 'systemMigrationVersion', version);
        }
    }
}