import * as Dice from "./sheets/dice.js";

export const tor1eUtilities = {};

/**
 * Take an array of values and split it in 2 part depending on the index in the array
 * @param abilities
 * @returns {*[]}
 * @private
 */
function _splitInTwo(abilities) {
    let lefts = abilities.filter((v, i) => !(i % 2));
    let rights = abilities.filter((v, i) => i % 2);
    return [lefts, rights]
}

/**
 * lore --> Lore, hunting --> Hunting
 * @param string
 * @returns {string}
 */
function _capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * Get itemId from an event by bubbling to the item class and get the id
 * @param event
 * @returns {string}
 * @private
 */
function _getActorId(event) {
    let element = event.currentTarget;
    return element.closest(".actor").dataset.actorId;
}

/**
 * Delete Actor from a list using its id
 * @returns {string}
 * @private
 * @param actor
 * @param _id
 * @param list
 * @param fromAttribute
 */
function _deleteActorFrom(actor, _id, list, fromAttribute) {
    let index = list.findIndex(function (element) {
        return element.id === _id;
    });
    if (index > -1) {
        list.splice(index, 1);
    }
    actor.update({[`${fromAttribute}`]: list})
}

/**
 * Add an actor to a List
 * @returns {string}
 * @private
 * @param actor
 * @param currentActor
 * @param list
 * @param fromAttribute
 */
function _addActorTo(actor, currentActor, list, fromAttribute) {
    list.push(currentActor);
    actor.update({[`${fromAttribute}`]: list})
}

/**
 * Get actorId from an event by bubbling to the actor class and get the id
 * @param event
 * @returns {string}
 * @private
 */
function _getItemId(event) {
    let element = event.currentTarget;
    return element.closest(".item").dataset.itemId;
}

/**
 *
 * @param element
 * @param extra
 * @returns {*}
 * @private
 */
function _getModifier(element, extra) {
    let actionBonusRaw = element.dataset.actionValueBonus;
    let actionBonus = (actionBonusRaw === undefined || actionBonusRaw === "") ? 0 : parseInt(actionBonusRaw);
    return (extra.favouredSkillValue && element.dataset.actionFavouredValue === "true")
        ? extra.favouredSkillValue + actionBonus
        : 0 + actionBonus;
}

/**
 *
 * @param actor
 * @param associateRawAttribute
 * @param actionFavouredValue
 * @returns {*}
 * @private
 */
function _getHopeModifier(actor, associateRawAttribute, actionFavouredValue) {
    let hopeModifier = 0;

    if (associateRawAttribute !== undefined && associateRawAttribute !== "") {
        let attribute = getProperty(actor, `system.attributes.${associateRawAttribute}`) || null;
        if (attribute !== undefined && attribute) {
            if (actionFavouredValue === "true") {
                hopeModifier = attribute.favoured.value;
            } else {
                hopeModifier = attribute.value;
            }
        }
    }

    return hopeModifier;
}

tor1eUtilities.combat = {
    "noActiveCombatInScene": function (_sceneId) {
        const sceneIds = Array.from(game.combats.values()).map(c => c.scene.id)
        return sceneIds.find(id => id === _sceneId) === undefined
    }
}

tor1eUtilities.macro = {
    "rollItemMacro": async function (_name, _type) {
        const speaker = ChatMessage.getSpeaker();
        let actor;
        if (speaker.token) actor = game.actors.tokens[speaker.token];
        if (!actor) actor = game.actors.get(speaker.actor);

        if (!actor) return ui.notifications.warn(game.i18n.localize("tor1e.macro.error.noActorAvailable"));

        let item = actor.extendedData.getItemFrom(_name, _type);

        if (!item) return ui.notifications.warn(game.i18n.format("tor1e.macro.error.itemIsMissing",
            {
                "itemName": _name,
                "itemType": _type,
                "actorName": actor.name,
            }));

        await actor.attackOpponentWith(_name);
    }
}

tor1eUtilities.filtering = {
    /**
     * Filter a list of items by its type (weapons, specialAbility, armour, ...) and its group ( for specialAbility for example, it could be Hate, Virtues, Valor)
     * This method returns left and right because it is used in a Two columns display.
     * @param items
     * @param type
     * @param subgroup
     * @returns {*[]}
     */
    "getAndSplitItemsBy": function (items, type, subgroup = null) {
        let elements = this.getItemsBy(items, type, subgroup);
        return _splitInTwo(elements);
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.filter(function (item) {
                return item.type === type;
            });
        } else {
            return items.filter(function (item) {
                return item.type === type && item.system.group.value === subgroup;
            });
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemsNot": function (items, type, subgroup) {
        return items.filter(function (item) {
            return item.type === type && item.system.group.value !== subgroup;
        });
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * @param items List of all items
     * @param type The type of items you want to filter
     * @param subgroup The subgroup of type of Item you want to filter (optional)
     * @returns {*}
     */
    "getItemBy": function (items, type, subgroup = null) {
        if (subgroup == null) {
            return items.find(function (item) {
                return item.type === type;
            });
        } else {
            return items.find(function (item) {
                return item.type === type && item.system.group.value === subgroup;
            });
        }
    },
    /**
     * Get the total encumbrance of the character
     * @param items List of all items
     * @returns {*}
     */
    "getEncumbrance": function (items) {
        return items.filter(function (item) {
            return item?.system?.encumbrance?.value > 0;
        }).map(function (item) {
            return item.system.encumbrance.value;
        }).reduce((a, b) => a + b, 0);
    },
    /**
     * Get all the equipped equipment
     * @param items List of all equipped items
     * @returns {*}
     */
    "getAllEquipedItems": function (items, isCharacter) {
        if (isCharacter) {
            return items.filter(
                item => item.isEquipped()
            );
        } else {
            return items;
        }
    },
    /**
     * Get a list of elements from Items filtered by the Type (Armour, weapon, special ability, ...)
     * The list of elements can be filtered by a list of groups. All the elements belonging to one of the groups.
     * @param items
     * @param type
     * @param groups
     * @param equipped
     * @returns {*}
     */
    "getItemIn": function (items, type, groups = null, equipped = false) {
        return items.find(function (item) {
            if (groups === null) {
                if (equipped && item.system.equipped) {
                    return item.type === type && item.system.equipped.value === true;
                } else {
                    return item.type === type;
                }
            } else {
                if (equipped && item.system.equipped) {
                    return item.type === type && groups.includes(item.system.group.value) && item.system.equipped.value === true
                } else {
                    return item.type === type && groups.includes(item.system.group.value)
                }
            }
        });
    }
}

tor1eUtilities.utilities = {
    /**
     *
     * @param fn
     * @param fallback
     * @returns {null|*}
     * @private
     */
    try: function (fn, fallback = null) {
        try {
            return fn();
        } catch (error) {
            return fallback
        }
    }
}

tor1eUtilities.rolling = {
    /**
     * Roll a skill test.
     * @returns {*}
     * @param token
     * @param skillName
     * @param actionName
     * @param automaticDifficultyRoll
     * @param associateRawAttribute
     * @param actionFavouredValue
     * @param difficulty
     * @param extra
     */
    "castCombatPreparationRoll": async function (token, skillName, actionName, automaticDifficultyRoll, associateRawAttribute, actionFavouredValue, difficulty = 14, extra = {}) {
        let actor = token.actor;
        let skill = getProperty(actor, `system.commonSkills.${skillName}`) || null;
        if (skill) {
            const roll = await Dice.taskCheck({
                actor: actor,
                user: game.user,
                actionValue: skill.value,
                difficulty: difficulty,
                actionName: actionName,
                askForOptions: automaticDifficultyRoll,
                wearyRoll: actor.getWeary(),
                hopeModifier: _getHopeModifier(actor, associateRawAttribute, actionFavouredValue),
                modifier: 0,
                hopePoint: tor1eUtilities.utilities.try(() => actor.system.resources.hope.value, 0),
                shadowServant: false,
                hopePointPostAction: extra.hopePointPostAction || {}
            });

            if (!roll || !extra.token) {
                return;
            }

            let data = extra.hopePointPostAction.options;

            const optionData = {
                actorName: data.actorName,
                actorId: data.actorId,
                token: {
                    id: extra.token.id,
                    name: extra.token.name,
                },
                difficulty: difficulty,
                combatId: extra.combatId
            }

            await actor.createBattleRollResultChatMessage(roll, optionData);
            return roll;
        } else {
            ui.notifications.warn(
                game.i18n.format("tor1e.roll.warn.noSkillFound",
                    {skillName: skillName, actorName: actor.name})
            );
        }
    },
}

tor1eUtilities.eventsProcessing = {
    async onToggleEffect(event) {
        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let effectId = element.dataset.effectId;

        if (event.altKey) {
            await actor.toggleStatusEffectById(effectId)
        }
    },
    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        $(element).siblings(".editor-container").toggle();
        $(element).toggleClass("expanded");
    },

    /**
     * Toggle the visibility of the block show/hide
     * @param event
     * @returns {*}
     */
    "onEditorToggle": async function (event) {
        event.preventDefault();
        let element = event.currentTarget;
        let editorWrapper = $(element).closest('li').children('.editor-container-toggelable');
        $(editorWrapper).toggleClass("show");
        $(editorWrapper).find(".editor-content").show();
        $(editorWrapper).find(".tox-tinymce").hide();
    },

    /**
     * Edit an item when you click on the edit icon
     * using a Popup window.
     * @param event
     * @returns {*}
     */
    "onItemEdit": async function (event) {
        event.preventDefault();
        let itemId = _getItemId(event);
        let item = this.actor.items.get(itemId);
        item.sheet.render(true);
    },

    /**
     * Open an actor from the list when you click on the edit button
     * @param event
     * @returns {*}
     */
    "onActorEdit": function (event) {
        event.preventDefault();
        let actorId = _getActorId(event);
        let actor = game.actors.get(actorId)
        actor.sheet.render(true);
    },

    /**
     * Delete an actor from the list when you click on the trash
     * @param extra
     * @param event
     * @returns {*}
     */
    "onActorDelete": function (extra = {}, event) {
        event.preventDefault();
        if (event.altKey) {
            let actorId = _getActorId(event);
            return _deleteActorFrom(this.actor, actorId, extra.list, extra.attribute);
        }
    },

    /**
     * Move an inline actor to the patron zone
     * @param extra
     * @param event
     * @returns {*}
     */
    "onExchangeZone": function (extra = {}, event) {
        event.preventDefault();
        if (event.altKey) {
            let actorId = _getActorId(event);
            let currentActor = extra.from.list.find(element => element.id === actorId)
            _addActorTo(this.actor, currentActor, extra.to.list, extra.to.attribute)
            return _deleteActorFrom(this.actor, actorId, extra.from.list, extra.from.attribute);
        }
    },

    /**
     * Update the value of an attribute of an inline actor in a list.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onInlineActorEdit": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;

        let actorId = _getActorId(event);
        let list = extra.list;
        let fromAttribute = extra.attribute;


        let value;
        // if the element is a checkbox then we have to translate the state (chack) into a boolean
        if (element.type === "checkbox") {
            value = element.checked === true;
        } else {
            value = element.value;
        }

        list.find(elt => elt.id === actorId).location = value;

        return this.actor.update({[`${fromAttribute}`]: list})
    },

    /**
     * Delete an item when you click on the trash
     * @param event
     * @returns {*}
     */
    "onItemDelete": async function (event) {
        event.preventDefault();
        if (event.altKey) {
            let itemId = _getItemId(event);
            return await this.actor.deleteEmbeddedDocuments("Item", [itemId]);
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemName": async function (extra = {}, event) {

        event.preventDefault();
        let element = event.currentTarget;
        let actor = this.actor;
        let itemId = _getItemId(event);
        let item = actor.items.get(itemId);
        let isActionFavoured = element.dataset.actionFavouredValue.toBoolean();

        if (event.altKey) {
            if (actor.extendedData.isCharacter && item.type === "weapon") {
                ui.notifications.warn(game.i18n.localize("tor1e.roll.warn.impossibleToFavouredAWeapon"));
                return undefined;
            }
            let field = element.dataset.actionFavouredName;
            return item.update({[field]: !isActionFavoured});
        } else {
            if ((actor.extendedData.isCharacter && item.type === "skill" && item.system.group.value === "combat")
                || (!actor.extendedData.isCharacter && item.type === "skill")) {
                let automaticDifficultyRoll = !event.shiftKey;
                let associateRawAttribute = element.dataset.associateAttributeName;
                let actionFavouredValue = element.dataset.actionFavouredValue;
                return Dice.taskCheck({
                    actor: actor,
                    user: game.user,
                    actionValue: element.dataset.actionValue,
                    actionName: item.name,
                    askForOptions: automaticDifficultyRoll,
                    wearyRoll: actor.getWeary(),
                    modifier: _getModifier(element, extra),
                    hopeModifier: _getHopeModifier(actor, associateRawAttribute, actionFavouredValue),
                    shadowServant: actor.extendedData.isHostile,
                    hopePoint: tor1eUtilities.utilities.try(() => actor.system.resources.hope.value, 0),
                });
            }

            let ownedItem = undefined

            if (item) ownedItem = actor.extendedData.getItemFrom(item.name, item.type);

            if (!ownedItem) return ui.notifications.warn(game.i18n.format("tor1e.macro.error.itemIsMissing",
                {
                    "itemName": _name,
                    "itemType": _type,
                    "actorName": actor.name,
                }));

            await actor.attackOpponentWith(ownedItem.name, {automaticDifficultyRoll: event.shiftKey});
        }
    },

    /**
     * Either update the a skill toggling the favoured state if you click with alt key.
     * Or roll against this skill if no other key is used.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillName": function (extra = {}, event) {
        event.preventDefault();
        let element = event.currentTarget;
        let associateRawAttribute = element.dataset.associateAttributeName;
        let actionFavouredValue = element?.dataset?.actionFavouredValue;
        let isActionFavoured = element?.dataset?.actionFavouredValue?.toBoolean();
        let actor = this.actor;

        if (event.altKey) {
            let field = element.dataset.actionFavouredName;
            return actor.update({[field]: !isActionFavoured});
        } else {
            let automaticDifficultyRoll = !event.shiftKey;
            Dice.taskCheck({
                actor: actor,
                user: game.user,
                actionValue: element.dataset.actionValue,
                actionName: element.dataset.actionName,
                askForOptions: automaticDifficultyRoll,
                wearyRoll: actor.getWeary(),
                modifier: _getModifier(element, extra),
                hopeModifier: _getHopeModifier(actor, associateRawAttribute, actionFavouredValue),
                shadowServant: actor.extendedData.isHostile,
                hopePoint: tor1eUtilities.utilities.try(() => actor.system.resources.hope.value, 0),
            });
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onItemSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.items.get(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ? parseInt(element.dataset.actionValue) : 0;
            return item.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let itemId = _getItemId(event);
            let item = this.actor.items.get(itemId);
            let field = element.dataset.field;
            let value = element.dataset.actionValue ? parseInt(element.dataset.actionValue) : 0;
            let max = parseInt(element.dataset.actionMaxValue);
            return item.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onSkillModify": function (extra = {}, event) {
        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : value});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            let max = parseInt(element.dataset.actionMaxValue);
            return this.actor.update({[field]: (value + 1 <= max) ? value + 1 : value});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param extra
     * @param event
     * @returns {*}
     */
    "onThreeStatesModify": function (extra = {}, event) {

        event.preventDefault();

        if (event.altKey && event.shiftKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value - 1 >= 0) ? value - 1 : 2});
        } else if (event.altKey) {
            let element = event.currentTarget;
            let field = element.dataset.field;
            let value = parseInt(element.dataset.actionValue);
            return this.actor.update({[field]: (value + 1 <= 2) ? value + 1 : 0});
        }
    },

    /**
     * Update the value of a stat when you change the value in the input text box.
     * Should be used for example when an item is in a sheet because the object doesn't belong to the proper stat of the sheet.
     * @param event
     * @returns {*}
     */
    "onSkillEdit": function (event) {
        event.preventDefault();

        let element = event.currentTarget;
        let itemId = _getItemId(event);
        let item = this.actor.items.get(itemId);
        let field = element.dataset.field;

        // if the element is a checkbox then we have to translate the state (check) into a boolean
        if (element.type === "checkbox") {
            return item.update({[field]: element.checked === true});
        }
        return item.update({[field]: element.value});
    },
}