import * as Dice from "../sheets/dice.js";
import Tor1eChatMessage from "./Tor1eChatMessage.js";
import {StatusEffects} from "../effects/status-effects.js";

export default function () {
    const template = "systems/tor1e/templates/chat/combat-damage-card.hbs";

    async function createChatMessageFrom(data, message, newState) {
        mergeObject(data, {state: newState});

        await message.setCombatantState(newState);

        await renderTemplate(template, data).then(html => {
            message.update({content: html})
        })
    }

    Hooks.on("getChatLogEntryContext", (html, options) => {

        function _getMessageAndState(a) {
            const {messageId} = a.data();
            const message = game.messages.get(messageId);
            const combatantState = message.getCombatantState();
            return {combatantState, message};
        }

        const canApplyDamage = (a) => {
            const {combatantState} = _getMessageAndState(a);
            return (game.user.isGM && combatantState && !combatantState.damageDealt);
        };
        const canApplyWounded = (a) => {
            const {combatantState} = _getMessageAndState(a);
            return (game.user.isGM && combatantState && combatantState.isWounded && !combatantState.woundedApplied);
        };
        const canApplyWeary = (a) => {
            const {message} = _getMessageAndState(a);

            const target = message.getCombatantTarget();

            if (!target) return false;

            const targetId = target.id;
            const targetCombatant = game.combat.getActiveCombatants().find(c => c.token.id === targetId);

            if (!targetCombatant.actor.extendedData.isCharacter) {
                return false
            }

            const damages = message.getCombatantDamages();

            const targetCombatantData = targetCombatant.actor;
            const endurance = targetCombatantData.extendedData.getEndurance();
            const fatigue = targetCombatantData.extendedData.getFatigue();

            return (game.user.isGM
                && !targetCombatant.actor.findStatusEffectById(StatusEffects.WEARY)
                && (fatigue > endurance - damages.total));
        };
        const canApplyOoc = (a) => {
            const {combatantState, message} = _getMessageAndState(a);

            if (!combatantState) return false;

            const target = message.getCombatantTarget();
            const targetId = target.id;
            const targetCombatant = game.combat.getActiveCombatants().find(c => c.token.id === targetId);

            const actor = targetCombatant.actor;
            const targetCombatantData = actor;
            const endurance = targetCombatantData.extendedData.getEndurance();

            const damages = message.getCombatantDamages();

            return (game.user.isGM
                && !actor.findStatusEffectById(StatusEffects.DEAD)
                && (endurance - damages.total <= 0
                    || ((combatantState.isWounded || false) &&
                        actor.hasFragileHealth())
                ));
        };

        async function applyDamage(a, data, targetCombatant) {
            if (!canApplyDamage(a)) return;

            let damages = data.damages;
            let targetCombatantData = targetCombatant.actor;
            let endurance = targetCombatantData.extendedData.getEndurance();
            let newEndurance = endurance - damages.total;
            await targetCombatantData.extendedData.updateEndurance(newEndurance < 0 ? 0 : newEndurance);

            return {
                damageDealt: true,
                effects: true,
            };
        }

        async function applyWeary(a, data, targetCombatant) {
            if (!canApplyWeary(a)) return;

            return {
                wearyApplied: true,
                isWeary: true,
                effects: true,
                statusEffect: StatusEffects.getStatusEffectBy(StatusEffects.WEARY)
            };
        }

        async function applyWounded(a, data, targetCombatant) {
            if (!canApplyWounded(a)) return;
            const {combatantState} = _getMessageAndState(a);

            return {
                woundedApplied: true,
                isWounded: combatantState.isWounded || false,
                effects: true,
                statusEffect: StatusEffects.getStatusEffectBy(StatusEffects.WOUNDED)
            };
        }

        async function applyOutOfCombat(a, data, targetCombatant) {
            if (!canApplyOoc(a)) return;


            return {
                oocApplied: true,
                isOoc: true,
                effects: true,
                statusEffect: StatusEffects.getStatusEffectBy(StatusEffects.DEAD)
            };
        }

        function _getMessageAndTarget(a) {
            const {messageId} = a.data();
            const message = game.messages.get(messageId);
            let data = Tor1eChatMessage.getExtendedData(message);
            let targetId = data.target.id;
            let targetCombatant = game.combat.getActiveCombatants().find(c => c.token.id === targetId);
            return {message, targetCombatant};
        }

        function _getMenuCallback(fns = []) {
            return async a => {
                let results = [];
                let {message, targetCombatant} = _getMessageAndTarget(a);
                let combatantState = message.getCombatantState();
                let data = Tor1eChatMessage.getExtendedData(message);

                for (let fn of fns) {
                    let effectResult = await fn(a, message.getExtendedData(), targetCombatant);
                    results.push(effectResult);
                }

                let statusEffectAcc = [];
                results
                    .filter((r) => r !== undefined)
                    .map((r, index) => {
                        if (combatantState === Object(combatantState)) {
                            //https://stackoverflow.com/questions/8511281/check-if-a-value-is-an-object-in-javascript
                            combatantState = mergeObject(combatantState, r);
                        }
                        if (r.statusEffect)
                            statusEffectAcc.push(r.statusEffect);
                    });

                combatantState.statusEffects = statusEffectAcc;
                await targetCombatant.actor.applyAllEffects(statusEffectAcc);
                await createChatMessageFrom(data, message, combatantState);
                if (combatantState.statusEffects.find(effect => effect.id === StatusEffects.DEAD)) {
                    await targetCombatant.update({defeated: true});
                }
            };
        }

        function addMenuItems(label, icon, fns = [], condition) {
            options.push({
                name: game.i18n.localize(`tor1e.combat.actions.${label}`),
                icon: `<i class="fas ${icon}"></i>`,
                callback: _getMenuCallback(fns),
                condition: condition
            });
        }

        //applyDamage should be the last function in the the fns array to be applied last !
        addMenuItems("applyDamage", "fa-tired", [applyDamage], canApplyDamage);
        addMenuItems("applyWeary", "fa-user-injured", [applyWeary], canApplyWeary);
        addMenuItems("applyWounded", "fa-heart-broken", [applyWounded], canApplyWounded);
        addMenuItems("applyOutOfCombat", "fa-skull", [applyOutOfCombat], canApplyOoc);
        addMenuItems("applyAllStates", "fa-list", [applyWeary, applyWounded, applyOutOfCombat], canApplyWeary || canApplyOoc || canApplyWounded);
        addMenuItems("applyAll", "fa-list-alt", [applyWeary, applyWounded, applyOutOfCombat, applyDamage], canApplyDamage || canApplyWeary || canApplyOoc || canApplyWounded);

        return options;
    });

    Hooks.on("renderChatLog", (app, html, data) => {
        _addChatListeners(html);
    });

    function _addChatListeners(html) {
        html.on('click', '.button-protection-roll', _onProtectionRoll);
        html.on('click', '.toggle', _onToggle);
    }

    function _onToggle(event) {
        event.preventDefault();
        event.stopPropagation();
        let element = event.currentTarget;
        let collapsibleElement = $(element).children('.collapsible');
        $(collapsibleElement).toggleClass("show");
    }

    async function _onProtectionRoll(event) {
        let automaticDifficultyRoll = false;
        if (event.shiftKey) {
            automaticDifficultyRoll = true;
        }

        event.preventDefault();
        event.stopPropagation();
        let element = event.currentTarget;
        let messageId = element.closest(".message").dataset.messageId;
        const message = game.messages.get(messageId);
        let data = Tor1eChatMessage.getExtendedData(message);
        let tokenId = data.target.id;
        let tn = element.dataset.tnTest;
        let combatant = game.combat.getActiveCombatants().find(c => c.token.id === tokenId);
        if (!game.user.isGM && !combatant.actor?.hasPlayerOwner) {
            return;
        }

        let actorData = combatant.actor;
        let protectionRoll = await Dice.taskCheck({
            actor: combatant.actor,
            user: game.user,
            difficulty: tn,
            askForOptions: !automaticDifficultyRoll,
            actionValue: actorData.extendedData.getArmourProtectionValue(),
            actionName: game.i18n.localize("tor1e.items.armours.protection-roll"),
            wearyRoll: combatant.actor.getWeary(),
            modifier: actorData.extendedData.getProtectionRollModifier(),
            hopeModifier: actorData.extendedData.getHopeModifier(),
            shadowServant: actorData.extendedData.isHostile,
            hopePoint: actorData.extendedData.getHopePoint(),
            hopePointPostAction: {
                fn: "rerollProtectionRoll",
                options: {
                    displayRoll: true,
                    messageId: message.id,
                    effects: true,
                    protectionRolled: true,
                },
            },
        });

        data = mergeObject(data, {
            state: {
                protectionRolled: true,
                isWounded: protectionRoll ? protectionRoll.isFailure() : false,
                effects: true,
            }
        });

        let chatData = {};
        chatData.content = await renderTemplate(template, data);
        chatData.flags = Tor1eChatMessage.buildExtendedDataWith(data);

        await ChatMessage.create(chatData);
    }
}